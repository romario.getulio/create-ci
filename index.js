var express = require('express')
var app = express()

app.get('/will', function (req, res) {
    res.send('{ "response": "Hello World" }')
})

app.get('/ready', function (req, res) {
    res.send('{ "response": "It works!" }')
})

app.listen(8000)
module.exports = app